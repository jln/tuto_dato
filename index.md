---
marp: true
theme: gaia
class:
    - invert

---

<!-- _class: invert lead 
_footer: v. 1.0 // 2021-10-03
-->

# À la découverte de dato
![](img/logo.png)

---

Voici la page d'accueil de dato à la première utilisation. C'est encore vide, mais on va vite remédier à ça :smile:

- l'espace vide est celui qui présentera nos bases de données (bdd)
- en bas, la barre bleue indique où l'on se trouve (`databases`)
- à droite, la barre d'outils verticale

![bg right](img/dato_home_empty.png)

---

Un point de **vocabulaire** avant de poursuivre :

- une *base de données* est une collection d'*entrées*
- une *entrée* est nécessairement d'un certain *type*
- un *type* d'entrée est composé de plusieurs *clés* qui le caractérise

Par exemple, au sein d'une bdd intitulée `contacts`, chaque entrée sera un contact particulier. Une entrée sera donc de type `contact`. Le type `contact` sera caractérisé par des clés comme `nom`, `prénom`, `téléphone`

---

# Étape 1 : créer un nouveau type d'entrée

La première étape consiste donc à créer un nouveau type d'entrée. Comme on veut créer une logithèque, nous allons logiquement créer un type intitulé `logiciel`

Pour ce faire, il faut cliquer sur ![](img/ico_a.png) ou utiliser le raccourci clavier `Maj + Espace`

---

![bg right](img/currently_opened_databases_empty.png)

La fenêtre qui s'ouvre alors, nous la verrons souvent. Elle affiche les bdd actuellement ouvertes (aucune pour le moment), l'icône ![](img/ico_home.png) qui permet d'accéder à la page d'accueil et l'icône ![](img/ico_types.png) sur laquelle nous allons cliquer, qui permet d'accéder aux différents types d'entrées.

---

![bg right](img/dato_types_empty.png)

Après avoir cliqué sur ![](img/ico_types.png), nous retrouvons une fenêtre à nouveau vide, puisque nous n'avons encore créé aucun type (notez qu'en bas à gauche, il est bien confirmé que nous nous trouvons sur la page `types`)

Un clic sur ![](img/ico_plus.png) nous mène à l'écran qui suit…

---

![bg right](img/new_type_or_method.png)

On clique sur `type`. La création de `method` n'est pas couverte par cette présentation.

---

![bg right](img/form_new_type.png)

Les choses intéressantes commencent maintenant ! :smile: Le formulaire de création d'un nouveau type comporte 4 grandes parties :

- `Type name` où l'on saisit le nom du type
- `Display` qui va permettre de régler l'affichage des entrées dans la base


---

![bg right](img/form_new_type.png)

- `Edit` où l'on va créer les clés caractérisant notre type
- `View` où l'on personnalise l'affichage d'une entrée individuelle

---

![bg right](img/form_new_type_name_filled.png)

On choisit un nom explicite : `logiciel`

:warning: Lorsque vous créerez d'autres types, évitez les noms comportant des caractères spéciaux ou des espaces, et privilégiez les minuscules

Puis on clique sur le champ noir correspondant à `Edit`: nous allons commencer à modeler notre type ! :tada:

---

![bg right](img/form_inputs_empty.png)

Une fois encore, on se retrouve logiquement avec un écran vide.

Un clic sur ![](img/ico_plus_black.png) permet de créer un nouveau champ.

---

Un nouveau point de vocabulaire : on parle de *champs* lorsqu'on désigne l'élément d'interface avec lequel l'utilisateur va interagir pour saisir des données : ce peut être un champ textuel court ou long, un curseur, une pipette de sélection de couleur, etc.

Un *champ* est nécessairement lié à une *clé*. La clé est le nom interne que l'on donne au champ, de façon à récupérer la valeur de celui-ci.

---

![bg right](img/form_inputs_1st_field_empty.png)

Après avoir cliqué sur ![](img/ico_plus_black.png), l'espace permettant de définir notre premier champ est créé. Mais il est encore vide : cliquons dessus pour le régler

---

![bg right](img/form_new_input_empty.png)

Avant de saisir quoi que ce soit, prenons le temps de découvrir cette fenêtre :

- `key` va recevoir le nom de la clé :warning: là encore, on va éviter les caractères spéciaux et les espaces
- `inputifyType` désigne le type de champ : champ textuel, curseur… nous en verrons quelques uns ensemble plus tard

---

![bg right](img/form_new_input_empty.png)

- `help` peut contenir un texte d'aide qui sera affiché si l'utilisateur laisse sa souris sur le nom du champ
- `mandatory` définit si le champ doit obligatoirement être saisi
- `label` est le nom du champ lisible par l'utilisateur
- `labelLayout` définit la position du `label` par rapport au champ
---

## :bulb: Précision

`key` va contenir un nom explicite, mais court. C'est une désignation technique du champ. `label` est un élément d'ergonomie, techniquement non indispensable au bon fonctionnement du système, mais facilitant grandement le quotidien de l'utilisateur final en proposant une description claire de chaque champ.

Par exemple, une fiche contact pourra proposer un champ permettant de saisir une adresse email personnelle et un autre, une adresse email professionnelle. Pour le premier champ, la `key` pourra être *mail_pro* et le `label` *Adresse email professionnelle* ; pour le second, la `key` *mail_perso* et le `label` *Adresse email personnelle*

---

![bg right](img/form_new_input_name_filled.png)

Notre premier champ va servir à stocker le nom du logiciel

- La `key` est intitulée *name*
- On laisse le type de champ par défaut (`normal`)
- `help` est laissé vide : le champ ne pose pas de difficulté particulière
- `mandatory` est activé : le champ est obligatoire

---

![bg right](img/form_new_input_name_filled.png)

- `label` contient un la dénomination explicite du champ : ici, *Nom du logiciel*
- enfin, on décide que le label sera situé au-dessus du champ de saisi (`stacked`). Si on les avait voulus sur la même ligne, on aurait choisi `inline`

Voilà notre premier champ créé ! :tada:

---


On pourrait désormais vouloir enregistrer notre travail en cliquant sur ![](img/ico_save.png). C'est tout à fait possible, mais on va plutôt cliquer sur ![](img/ico_left_arrow.png) (en haut à gauche) pour créer immédiatement deux autres champs :

- un champ qui permettra de saisir une description du logiciel
- un autre qui permettra de saisir la ou les plateforme(s) sur laquelle ou lesquelles tourne le logiciel

---

![bg right](img/form_inputs_1st_field_created.png)

On revient donc à l'écran listant tous les champs d'édition, où l'on constate que le champ nouvellement créé est détaillé par son code au format `json`

On clique deux fois sur ![](img/ico_plus_black.png) pour poursuivre la création de nos deux autres champs.

---

![bg right](img/form_new_input_desc_filled.png)

La capture devrait vous être compréhensible : on crée une `key` nommée *desc*, avec un `label` plus explicite, *Description*

Ce champ ne sera pas obligatoire.

Ce qu'il faut tout de même noter, c'est le type de champ : ici, `textarea` autorisera la saise d'une description sur plusieurs lignes

---

![bg right](img/form_new_input_plateform_filled.png)

Après avoir cliqué sur ![](img/ico_left_arrow.png) puis sur le dernier espace vide, on saisit les infos relatives au champ `plateform`.

À noter ici : l'utilisation du type `multi` qui permettra à l'utilisateur de saisir et même de créer de nouvelles plateformes "à la volée". Le texte d'aide précie à celui-ci qu'il peut saisir plusieurs plateformes.

---

![bg right](img/form_new_inputs_three_fields_created.png)

Après un clic sur ![](img/ico_left_arrow.png), on devrait se retrouver avec cet écran. Voilà qui est pas mal du tout ! On va pouvoir commencer à alimenter notre logithèque. Avant cela, n'oublions pas de sauvegarder ce nouveau type `logiciel` en cliquant sur ![](img/ico_save.png) puis sur le bouton `Save and close`

---

![bg right](img/dato_types_logiciel_created.png)

De retour sur les `types`, on constate que notre type `logiciel` est bel et bien créé. On peut cliquer dessus à tout moment pour le modifier.

---

## Étape 2 : créer la bdd
Mainteant, que notre premier type est créé, on doit désormais créer notre première base de données. Pour ce faire, on clique sur ![](img/ico_a.png) (ou `Maj + Espace`) puis ![](img/ico_home.png) pour revenir à l'accueil.

---

![bg right](img/form_new_db_filled.png)

De retour sur l'écran d'accueil, on crée une nouvelle bdd en cliquant sur ![](img/ico_plus.png) puis en choisissant `database`. Une nouvelle fenêtre s'ouvre, qu'on remplit comme le montre la capture

---

![bg right](img/form_new_db_filled.png)

C'est très simple, on se contente :

- de lui donner un nom, ici *logitheque*
- de sélectionner les types d'entrée qu'elle pourra contenir, ici le type *logiciel* que nous venons de concevoir

On sauvegarde en cliquant sur l'icône disquette.

---

![bg right](img/dato_home_logitheque_created.png)

Désormais, la fenêtre `databases` affiche bien notre bdd nouvellement créée.

Plus précisément, que voit-on ?

- une icône par défaut
- le nom de la base
- le type d'entrées utilisé par cette base. Si plusieurs types sont utilisés, ils seront tous affichés

---

![bg right](img/logitheque_empty.png)

En double cliquant sur la base, on aboutit à la fenêtre ci-contre qui nous indique qu'aucune entrée n'est encore créée.

Ne perdons pas davantage de temps, et créons donc notre première entrée en cliquant sur ![](img/ico_plus.png)

---

![bg right](img/form_new_logiciel_empty.png)

Le formulaire de saisie s'ouvre tel qu'on pouvait s'y attendre, en nous proposant les 3 champs précédemment créés. Le seul champ obligatoire (*Nom du logiciel*) apparaît sur fond orange.

On constate que les trois "labels" figurent bien *au-dessus* des champs puisqu'on avait choisi la position `stacked`

--- 

![bg right](img/form_new_logiciel_firefox.png)

Saisissons les informations relatives, par exemple, au logiciel *Firefox*. Puis cliquons sur ![](img/ico_save.png) puis sur `save and close` afin de revenir à la base *logitheque*.

---

![bg right](img/logitheque_with_firefox.png)

De retour sur l'écran principal de notre base, nous voyons bien l'entrée  créée, avec une icône par défaut.

Le nom du logiciel  y est affiché car nous avons utilisé une clé intitulée *name*. Si nous avions utilisé un autre nom , il n'apparaîtrait pas.

Par ailleurs, on ne sait pas quelles plateformes sont compatibles.

---

![bg right](img/view_firefox_empty.png)

En double cliquant sur l'entrée, on obtient… pas grand chose. Si ce n'est un message nous enjoignant à cliquer sur ![](img/ico_edit_settings.png) pour modifier la `view`.

---

Récapitulons : l'entrée est bien créée mais :

* l'affichage de l'entrée dans la base pourrait être grandement améliorée
    * on pourrait vouloir afficher le logo de Firefox plutôt qu'une icône par défaut
    * et également, afficher les plateformes compatibles
* la visualisation de l'entrée est encore moins satisfaisante, puisque totalement vide. On s'attend plutôt à pouvoir y lire les informations enregistrées.

Corrigeons donc tout cela en modifiant le `display` et la `view` du type d'entrée *logiciel*. Cliquons donc sur ![](img/ico_edit_settings.png)

---

:bulb: Cliquer sur ![](img/ico_edit_settings.png) est un raccourci pour éditer un type particulier. On pourrait tout à fait cliquer sur ![](img/ico_a.png), puis ![](img/ico_types.png), puis sur le type *logiciel* pour l'éditer, mais on voit bien ici que ce serait inutilement long de procéder ainsi.

---

![bg right](img/form_logiciel_display_and_view_empty.png)

Après avoir cliqué sur ![](img/ico_edit_settings.png), on retrouve une fenêtre que l'on connaît, qui nous confirme bien que ni `display` ni `view` n'ont été paramétrés jusqu'à présent.

Mais avant de les paramétrer, paramétrons à nouveau `Edit` de façon à pouvoir ajouter un logo à un logiciel.

---

![bg right](img/form_new_input_logo_filled.png)

Après avoir cliqué sur ![](img/ico_plus_black.png) puis sur le champ vide, on retrouve le formulaire de création d'un nouvel `input` qu'on remplit comme suit. Notez en particulier :

* `inputifyType` est réglé à `file` : un logo est en effet un fichier qu'on va associer à l'entrée
* dans `file`, on précise que le fichier est de type `image`

---

![bg right](img/form_logiciel_with_logo.png)

En cliquant à 2 reprises sur ![](img/ico_left_arrow.png) ou directement sur ![](img/ico_curly_braces.png), on voit dans `Edit` que l'ajout d'un champ `logo` a bien été pris en compte.

On peut désormais paramétrer `Display` en cliquant dessus.

---

![bg right](img/form_display_empty.png)

5 éléments sont paramétrables :
* le titre de l'entrée
* le sous-titre
* sa couleur
* son icône
* son image

Même si le titre est déjà affiché (*Firefox* dans notre exemple), paramétrons cela proprement

---

![bg right](img/form_display_title_empty.png)

Il nous faut désormais choisir une `method`, c'est-à-dire *grosso modo* une fonction, grâce à laquelle on va déterminer quelle valeur utiliser pour définir ce qu'est le titre d'une entrée.

On clique  sur `Method` puis on choisit `valueOrResult` : il s'agit de la méthode la plus simple pour récupérer la valeur d'un champ.

---

![bg right](img/form_display_title_filled.png)

La méthode `valueOrResult` réclame un `argument` de type `deepKey`.

Pour le moment, considérons que `deepKey` est parfaitement identique à `key`.

Nous saisissons donc la `key` contenant le titre du logiciel dans une entrée du même nom. Dans notre exemple, c'est *name*

---

:bulb: On voit que la méthode `valueOrResult` demande un second argument intitulé `fallbackTitle` : c'est la valeur à utiliser si dato ne trouve aucun donnée dans la clé indiquée.

Cet argument est optionnel, et comme on avait pris soin de rendre le champ associé à la clé *name* obligatoire, il y aura toujours une valeur associée à cette clé. On décide donc de laisser `fallbackTitle` vide.

---

![bg right](img/form_display_with_title.png)

Désormais, vous avez compris le principe : en revenant en arrière (clic sur ![](img/ico_left_arrow.png)) on constate que `title` est paramétré.

Définissons désormais le `subtitle` : nous voulons que ce soit les plateformes compatibles avec un logiciel.

---

![bg right](img/form_display_subtitle_filled.png)

Pour le sous-titre, nous allons utiliser une autre méthode, `joined`.

Un argument doit être saisi, la clé contenant les plateformes compatibles. Nous avions nommé cette clé *plateform*.

---

:bulb: La méthode précédemment utilisée `valueOrResult`, est à employer avec une chaîne de caractères simple.

Or, la clé `plateform` est d'un type particulier nommé `array` : elle comporte *plusieurs* valeurs, indépendantes (ex : *Linux*, *Windows*, *Android*)

La méthode `joined` permet de regrouper ces valeurs.

---

![bg right](img/form_display_with_subtitle.png)

Le titre et le sous-titre sont désormais paramétrés. Reste le logo, que l'on souhaite afficher. Pour ce faire, on clique sur `image`

---

![bg right](img/form_display_image_filled.png)

La méthode à utiliser ici est `imageAttachedEntry` : en effet, nous avons précédemment ajouté une nouvelle clé de type `file`, clé que nous avions nommée *logo*. C'est donc logiquement ce nom de clé qu'il faut indiquer comme argument.

Notre `display` est désormais paramétré : on clique sur ![](img/ico_save.png) et on se rend à nouveau dans notre base *logitheque*.

---

![bg right](img/logitheque_with_firefox.png)

De retour dans notre bdd, rien n'a changé. C'est normal puisqu'on n'a pas rafraîchit la base pour qu'elle prenne en compte nos modifications.

Pour ce faire, on clique sur ![](img/ico_dato.png) puis `Reload this database`.

---

![bg right](img/logitheque_with_firefox_subtitled.png)

:tada: Les plateformes sont bien affichées en sous-titre !

Mais reste le logo ! Il nous faut l'ajouter à l'entrée *Firefox*. Pour éditer l'entrée, on la sélectionne d'un simple clic puis on clique sur ![](img/ico_plume.png)

---

![bg right](img/form_edit_firefox_logo.png)

On voit qu'un nouveau champ *logo* a été ajouté. Au passage, remarquons qu'en l'absence de `label`, dato utilise automatiquement le nom de la clé.

En cliquant sur `choose from file`, on choisit un fichier image sur son disque, puis on sauvegarde avec ![](img/ico_save.png)

---

![bg right](img/logitheque_with_firefox_logo.png)

re-:tada: ! Le logo de Firefox apparaît désormais bien pour afficher l'entrée !

En ouvrant à nouveau l'entrée et en cliquant sur ![](img/ico_edit_settings.png) pour éditer le type *logiciel*, on va désormais s'attaquer à la visualisation (`View`) de celui-ci.

---

![bg right](img/form_view_empty.png)


Après avoir cliqué sur `View`, on aboutit au formulaire encore vide présenté ci-contre.

dato fournit un bouton très pratique `autofill fields from edit keys` qui en un clic va ajouter toutes nos clés précédemment créées au formulaire.

---

![bg right](img/form_view_empty_autofilled.png)

Nos 4 clés sont donc désormais ajoutées. Avant de les peaufiner individuellement, on clique sur ![](img/ico_reorder.png) pour ré-ordonner l'affichage de ces clés.

---

![bg right](img/form_display_reorder_keys.png)

Par simple glisser/déposer, on repositionne la clé *logo* sous la clé *name* puis on valide.

Passons ensuite à l'édition individuelle des 4 clés.

---

![bg right](img/form_view_name.png)

On retrouve à quelques détails près le formulaire d'édition des clés. Comme on le voit, une clé peut être affichée *via* `custom display` selon une `method` spécifique, être associée à un `label`, lui-même pouvant être caché, positionné au-dessus de la valeur à afficher, ou sur la même ligne.

Pour la clé *name*, ne touchons à rien !

---

![bg right](img/form_view_logo.png)

Pour la clé logo, on va devoir modifier `custom display` : il s'agit en effet d'afficher une image, ce qui nécessite l'usage d'une méthode particulière : `attachedImage`

C'est le seul paramètre à modifier pour cette clé ; le reste peut être laissé par défaut

---

![bg right](img/form_view_desc.png)

Pour l'affichage de la description, on choisit d'afficher un `label`, *Description*. Celui-ci sera située au-dessus du texte de la description (`stacked`)

---

![bg right](img/form_view_plateforms.png)

Pour la clé `plateform`, dont on a vu la spécificité plus haut (elle est de type `array`), on choisit une méthode d'affichage particulière : `tagsFilteringDatabaseOnClick` Ainsi, chaque plateforme apparaîtra comme une étiquette cliquable, qui filtrera les entrées de la base.

Le label *Plateformes* est lui aussi positionné avec `stacked`

---

![bg right height:90%](img/view_firefox.png)

Après avoir enregistré les paramétrages de `view`, être revenu à la bdd *logitheque* et avoir rechargé cette dernière comme on l'a précédemment fait (clic sur ![](img/ico_dato.png) puis `Reload this database`), on peut admirer notre résultat !

---

Voilà, la première partie du tuto se termine ici ! Nous y avons appris : 

* les notions de base de dato : entrées, types, clé, base de données, label…
* la création de bases, de types d'entrées et d'entrées
* le paramétrage des types (`display`, `edit` et `view`)
* quelques types de clés : `normal`, `textarea`, `multi` et `file`
* l'utilisation de quelques méthodes d'affichage  : `joined` et `imageAttachedEntry` pour la parte `display` et `attachedImage` et `tagsFilteringDatabaseOnClick` pour la partie `view` 

